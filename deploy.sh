#!/bin/bash

HOST1="35.204.53.53"
USERNAME1="test2"
PASSWORD1="test"
DATABASE1="testA"

HOST2="35.204.53.53"
USERNAME2="test2"
PASSWORD2="test"
DATABASE2="testB"

mysqldump --skip-comments --skip-extended-insert -h $HOST1 -u $USERNAME1 --password="test" $DATABASE1>dump.sql
mysqldiff --server1=$USERNAME1:$PASSWORD1@$HOST1 --server2=$USERNAME2:$PASSWORD2@$HOST2 --changes-for=server2 --force --difftype=sql  $DATABASE1:$DATABASE2  --skip-table-options > diff.sql

php compiler.php

mysql -u $USERNAME2 --password="test" -h $HOST2 $DATABASE2 < schema_changes.sql
#rm file1.sql
#rm file2.sql

