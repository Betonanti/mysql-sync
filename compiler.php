<?php


class Compiler{

    const SCHEMA_DUMP = ["file" => "dump.sql","var" => "dump"];
    const SCHEMA_DIFF = ["file" => "diff.sql","var" => "diff"];
    const OUTPUT = "schema_changes.sql";
    const DATABASE1 = "c1periscope";
    const DATABASE2 = "c1prod_ppms";
    const BACKUP = "logs/schema_changes";

    public $dump, $diff, $output = [], $tables = [], $views, $alerts = [];

    public function __construct()
    {
        $this->read(self::SCHEMA_DUMP);
        $this->read(self::SCHEMA_DIFF);
    }


    public function read($file){
        $this->check_file($file['file']);

        $f = fopen($file['file'],'r');
        $fread = fread($f,filesize($file['file']));
        fclose($f);

        $var = $file['var'];
        if($file['var'] == "dump"){
            $this->$var = explode(";",$fread);
        }else{
            $this->$var = explode("\n",$fread);
        }

    }

    public function getTables(){

        $index = "create";
        foreach($this->diff as $key => $row){

            if(preg_match("#\s{8}\TABLE: (.*)#",$row,$response) === 1){
                $this->tables[$index][] = $response[1];
            }else if(preg_match("#\s{8}\VIEW: (.*)#",$row,$response) === 1){
                $this->views[$index][] = $response[1];
            }else{
                if(preg_match("#server[0-9]*.(".self::DATABASE2."):#",$row) === 1){
                    $index = "create";
                }
                if(preg_match("#server[0-9]*.(".self::DATABASE1."):#",$row) === 1){
                    $index = "drop";
                }

                if(@$row[0] !== "#"){
                    $row = $this->replacer($row);
                    $this->alerts[] = $row;
                }

                unset($this->diff[$key]);
            }
        }
        unset($this->alerts[count($this->alerts)-2]);
        unset($this->alerts[count($this->alerts)-1]);
    }

    public function generate(){

        $this->getCreate();
        $this->getDrop();


        $this->output = array_merge($this->output,$this->alerts);
        $this->create();
    }

    private function create(){

        foreach($this->output as $key => $item){
            if(strlen($item) == 0){
                unset($this->output[$key]);
            }
        }

        $f = fopen(self::OUTPUT,'w');
        fwrite($f,implode("\n",$this->output));
        fclose($f);


        //LOG
        $f = fopen(self::BACKUP.date('YmdHis',time()).".sql",'w');
        fwrite($f,implode("\n",$this->output));
        fclose($f);
    }

    private function getCreate(){

        foreach($this->dump as $key => $row){
            if(isset($this->tables['create'])) {
                foreach ($this->tables['create'] as $table) {
                    if (preg_match('#CREATE TABLE \`(' . $table . ')\`#', $row) === 1) {
                        $this->output[] = $row . ";";
                        break;
                    }
                }
            }
            if(isset($this->views['create'])) {
                foreach ($this->views['create'] as $view) {
                    if (preg_match('#VIEW \`(' . $view . ')\`#', $row) === 1) {
                        $this->output[] = "CREATE " . str_replace("*/", " ", substr($row,strpos($row,'VIEW'))) . ";";
                        break;
                    }
                }
            }

        }
    }

    private function getDrop(){
        if(isset($this->tables['drop'])){
            foreach($this->tables['drop'] as $table){
                $this->output[] = "DROP TABLE ".$table.";";
            }
        }

        if(isset($this->views['drop'])){
            foreach($this->views['drop'] as $view){
                $this->output[] = "DROP VIEW ".$view.";";
            }
        }

    }

    private function check_file($file){
        if(!file_exists($file)){
            throw new Exception("not found the file ".$file);
        }
    }

    private function replacer($string){

        $string = str_replace(self::DATABASE1,self::DATABASE2,$string);
        $string = str_replace(self::DATABASE1."@%",'',$string);
        $string = preg_replace('/(DEFINER=(.*)@(.*))\s(VIEW.*)/', '$4', $string);
        $string = preg_replace('/ALTER TABLE/', '$1  ALTER TABLE IGNORE', $string);


        if(preg_match('/(DEFINER=(.*)@(.*))\s(VIEW)\s\w{1,50}.\w{1,50};/', $string, $output_array) === 1){
            $string = "";
        }


        return $string;
    }



}

$c = new Compiler();
$c->getTables();
$c->generate();


?>



